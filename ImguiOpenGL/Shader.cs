﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OpenGL;

namespace ImguiOpenGL
{
	/// <summary>
	/// Definition for completion with OpenGL tutorial.
	/// write my own later, along with vertex 
	/// </summary>
	class Shader
	{
		public uint ID;

		public Shader(string computePath)
		{
			string computeCode = File.ReadAllText(computePath);

			int[] success = new int[1];

			uint compute = Gl.CreateShader(ShaderType.ComputeShader);
			Gl.ShaderSource(compute, computeCode);
			Gl.CompileShader(compute);
			Gl.GetShaderiv(compute, ShaderParameter.CompileStatus, success);
			if(success[0] != 1)
			{
				string error = Gl.GetShaderInfoLog(compute);
				Console.WriteLine(error);
			}
			//thats all that.
			ID = Gl.CreateProgram();
			Gl.AttachShader(ID, compute);
			Gl.LinkProgram(ID);

			Gl.GetProgramiv(ID, ProgramParameter.LinkStatus, success);
			if(success[0] != 1)
			{
				string error = Gl.GetProgramInfoLog(ID);
				Console.WriteLine(error);
			}
			Gl.DeleteShader(compute);
		}

		public Shader(string vertexPath, string fragmentPath)
		{
			string vertexCode = File.ReadAllText(vertexPath);
			string fragmentCode = File.ReadAllText(fragmentPath);

			uint vertex, fragment;
			int[] success = new int[1];


			vertex = Gl.CreateShader(ShaderType.VertexShader);
			Gl.ShaderSource(vertex, vertexCode);
			Gl.CompileShader(vertex);

			Gl.GetShaderiv(vertex,ShaderParameter.CompileStatus, success);
			
			if(success[0] != 1)
			{
				string error = Gl.GetShaderInfoLog(vertex);
				Console.WriteLine(error);
			}

			fragment = Gl.CreateShader(ShaderType.FragmentShader);
			Gl.ShaderSource(fragment, fragmentCode);
			Gl.CompileShader(fragment);

			Gl.GetShaderiv(fragment, ShaderParameter.CompileStatus, success);
			if(success[0] != 1)
			{
				string error = Gl.GetShaderInfoLog(fragment);
				Console.WriteLine(error);				
			}

			ID = Gl.CreateProgram();
			Gl.AttachShader(ID, vertex);
			Gl.AttachShader(ID, fragment);
			Gl.LinkProgram(ID);

			Gl.GetProgramiv(ID, ProgramParameter.LinkStatus, success);
			if(success[0] != 1)
			{
				string error = Gl.GetProgramInfoLog(ID);
				Console.WriteLine(error);
			}

			Gl.DeleteShader(vertex);
			Gl.DeleteShader(fragment);
		}
		public void use()
		{
			Gl.UseProgram(ID);
		}
		public void setBool(string name, bool value)
		{
			Gl.Uniform1i(Gl.GetUniformLocation(ID, name), value ? 1 : 0);
		}
		public void setInt(string name, int value)
		{
			Gl.Uniform1i(Gl.GetUniformLocation(ID, name), value);
		}
		public void setFloat(string name, float value)
		{
			Gl.Uniform1f(Gl.GetUniformLocation(ID, name), value);
		}
		public void setMatrix(string name, Matrix4 value)
		{
			Gl.UniformMatrix4fv(Gl.GetUniformLocation(ID, name), value);
			
		}
	}
}
