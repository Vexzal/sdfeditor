﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GLFW;
using OpenGL;
using ImGuiNET;
using System.Numerics;

namespace ImguiOpenGL
{
	class Program
	{
		static void Main(string[] args)
		{
			using (var app = new Environment())
			{
				app.Run();
			}
		}
	}

	class Environment : IDisposable
	{
		imguiRenderer imgui;
		Window window;
		Vector2 Resolution;

		public void Run()
		{
			Glfw.Init();
			Resolution = new Vector2(1600, 960);
			Glfw.WindowHint(Hint.ClientApi, ClientApi.OpenGL);
			Glfw.WindowHint(Hint.ContextVersionMajor, 4);
			Glfw.WindowHint(Hint.ContextVersionMinor, 3);
			Glfw.WindowHint(Hint.OpenglProfile, Profile.Core);
			
			window = Glfw.CreateWindow((int)Resolution.X, (int)Resolution.Y, "Editor", Monitor.None, Window.None);
			Glfw.SetWindowSizeLimits(window, (int)Resolution.X, (int)Resolution.Y, (int)Resolution.X, (int)Resolution.Y);
			Glfw.MakeContextCurrent(window);
			Gl.Viewport(0, 0, (int)Resolution.X, (int)Resolution.Y);
			Glfw.SetKeyCallback(window, KeyboardCallback);
			//frame, screw it turn off resizing.
			imgui = new imguiRenderer();

			while (!Glfw.WindowShouldClose(window))
			{
				Glfw.PollEvents();
				Update();
				Render();
				Glfw.SwapBuffers(window);
			}
		}
		public void Update()
		{

		}
		public void Render()
		{
			Gl.ClearColor(1f, .5f, .5f, 1f);
			Gl.Clear(ClearBufferMask.ColorBufferBit);

			imgui.BeforeLayout();

			ImGui.Begin("TitleWindow");
			ImGui.LabelText("Hello World", "");
			ImGui.End();


			imgui.AfterLayout();

		}
		Vector2 CursorPosition = new Vector2();
		public void ProcessInput(Window window)
		{
			//hmm. I might just grab input whenever instead of cycling the whole frame into values.
			//mainly because I don't want to do otherwise.
			double x, y;
			Glfw.GetCursorPosition(window, out x, out y);
			CursorPosition = new Vector2((float)x, (float)y);

			
			
		}
		
		public void KeyboardCallback(IntPtr window,Keys keyCode, int scanCode, InputState inputState, ModifierKeys modifierKeys)
		{
			
		}
		
		public void Dispose()
		{
			
		}
	}
}
