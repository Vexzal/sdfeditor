﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenGL;
using ImGuiNET;
using System.Numerics;
using System.Runtime.InteropServices;

namespace ImguiOpenGL
{
	public class imguiRenderer
	{
		//I know I'm missing vertex declarations
		uint VAO;
		uint VBO;
		uint EBO;

		uint FontTexture;

		Shader GuiShader;
		Matrix4x4 Projection;
		
		//constructor do setup.
		public imguiRenderer()
		{
			var context = ImGui.CreateContext();
			ImGui.SetCurrentContext(context);

			var fonts = ImGui.GetIO().Fonts;
			ImGui.GetIO().Fonts.AddFontDefault();

			GuiShader = new Shader(@"\Shaders\imgui.vert", @"\Shaders\imgui.frag");

			//do all my loading in here.
			ImDrawVert vert = new ImDrawVert();
			int vertexStride = sizeof(float) * 4 + sizeof(byte);
			
			VAO = Gl.GenVertexArray();
			VBO = Gl.GenBuffer();
			EBO = Gl.GenBuffer();
			Gl.BindVertexArray(VAO);
			Gl.BindBuffer(BufferTarget.ArrayBuffer, VBO);
			Gl.BindBuffer(BufferTarget.ElementArrayBuffer, EBO);
			Gl.VertexAttribPointer(0, 2, VertexAttribPointerType.Float, false, vertexStride, (IntPtr)0);
			Gl.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, vertexStride, (IntPtr)(sizeof(float) * 2));
			Gl.VertexAttribPointer(2, 4, VertexAttribPointerType.UnsignedByte, true, vertexStride, (IntPtr)(sizeof(float) * 4));
			Gl.EnableVertexAttribArray(0);
			Gl.EnableVertexAttribArray(1);
			Gl.EnableVertexAttribArray(2);
			//hmm.
			//define 
			Gl.BindVertexArray(0);



			ImGui.NewFrame();


		}
		

		//create an id for a texture.
		public IntPtr BindTexture(uint TextureId)
		{
			return (IntPtr)TextureId;
		}
		
		//binds the font atlas to the gpu.
	    void BindAtlas()
		{
			var io = ImGui.GetIO();
			io.Fonts.GetTexDataAsRGBA32(out IntPtr pixelData, out int width, out int height, out int bytesPerPixel);

			//io.Fonts.SetTexID((IntPtr))

			FontTexture = Gl.GenTexture();
			Gl.BindTexture(TextureTarget.Texture2D,FontTexture);
			Gl.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba32f, width, height, 0, PixelFormat.Rgba, PixelType.Float, pixelData);
			Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, TextureParameter.ClampToEdge);
			Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, TextureParameter.ClampToEdge);
			Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, TextureParameter.Linear);
			Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, TextureParameter.Linear);
			Gl.BindTexture(TextureTarget.Texture2D, 0);
			//so I've bound the texture to the device now what.
			//I think this is adequate.
			io.Fonts.SetTexID((IntPtr)FontTexture);
			io.Fonts.ClearTexData();
			//I want to.
			
		}
		//so for now I'll copy about exactly.
		//but in general  Iwant to seperate out my input and rendering.
		public void BeforeLayout()
		{
			ImGui.GetIO().DeltaTime = (float)GLFW.Glfw.Time;
			//delta time.
			//update input
			//input later.
			ImGui.NewFrame();
		}
		public void AfterLayout()
		{
			ImGui.Render();
			RenderDrawData(ImGui.GetDrawData());
			
		}
		void RenderDrawData(ImDrawDataPtr drawData)
		{
			//alright so. here is some interesting things I need to set up render info.



			//alright lets go over what this is and how I need to do it.
			//loading for me is //this function largely just centralizes the drawing steps like.
			UpdateBufers(drawData);
			RenderCommands(drawData);



		}
		//update buffers temp done. loop over cmd lists and render!
		void UpdateBufers(ImDrawDataPtr drawData)
		{
			if (drawData.TotalVtxCount == 0)
				return;

			Gl.BindBuffer(BufferTarget.ArrayBuffer, VBO);
			Gl.BindBuffer(BufferTarget.ElementArrayBuffer, EBO);
			int vtxOffset = 0;
			int idxOffset = 0;
			/**
			//vertexData = new byte[drawData.TotalVtxCount * Marshal.SizeOf(typeof(ImDrawVert))];
			//indexData = new byte[drawData.TotalIdxCount * sizeof(ushort)];
			////the next bit in the demo is just updating size of the buffers,I 'm ignoring that.

			
			//for (int i = 0;i<drawData.CmdListsCount;i++)
			//{
			//	ImDrawListPtr cmdList = drawData.CmdListsRange[i];
			//	fixed (void* vtxDstPtr = &vertexData[vtxOffset * Marshal.SizeOf(typeof(ImDrawVert))])
			//	fixed (void* indxDstPtr = &indexData[idxOffset * sizeof(ushort)])
			//	{
			//		Buffer.MemoryCopy((void*)cmdList.VtxBuffer.Data, vtxDstPtr, vertexData.Length, cmdList.VtxBuffer.Size * Marshal.SizeOf(typeof(ImDrawVert)));
			//		Buffer.MemoryCopy((void*)cmdList.IdxBuffer.Data, indxDstPtr, indexData.Length, cmdList.IdxBuffer.Size * sizeof(ushort));
			//	}
			//	vtxOffset += cmdList.VtxBuffer.Size;
			//	idxOffset += cmdList.IdxBuffer.Size;
			//}
			//so instead of doing all that I could do.
			**/
			Gl.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(drawData.TotalVtxCount * Marshal.SizeOf(typeof(ImDrawVert))), IntPtr.Zero, BufferUsageHint.StreamDraw);
			Gl.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(drawData.TotalIdxCount * sizeof(ushort)), IntPtr.Zero, BufferUsageHint.StreamDraw);
			//then a foreach commands.

			for(int i = 0;i<drawData.CmdListsCount;i++)
			{
				var cmdList = drawData.CmdListsRange[i];
				Gl.BufferSubData(BufferTarget.ArrayBuffer, (IntPtr)(vtxOffset * Marshal.SizeOf(typeof(ImDrawVert))), (IntPtr)(cmdList.VtxBuffer.Size * Marshal.SizeOf(typeof(ImDrawVert))), cmdList.VtxBuffer.Data);
				Gl.BufferSubData(BufferTarget.ElementArrayBuffer, (IntPtr)(idxOffset * sizeof(ushort)), (IntPtr)(cmdList.IdxBuffer.Size * sizeof(ushort)), cmdList.IdxBuffer.Data);

				vtxOffset += cmdList.VtxBuffer.Size;
				idxOffset = cmdList.IdxBuffer.Size;
			}
			//that should set all my data nicely.
			//now I just need to draw it.

			//hmm.//so I'm copying to arrays but I think within the form I could just pass data directly with.
			

		}
		void RenderCommands(ImDrawDataPtr drawData)
		{
			//int vtxOffset = 0;
			int offset = 0;
			
			for(int list = 0;list < drawData.CmdListsCount;list++)
			{
				ImDrawListPtr cmdList = drawData.CmdListsRange[list];
				for(int cmd = 0;cmd < cmdList.CmdBuffer.Size;cmd++)
				{
					var drawCmd = cmdList.CmdBuffer[cmd];
					//the thing here checks for a loaded texture.
					Gl.Scissor((int)drawCmd.ClipRect.X,
						(int)drawCmd.ClipRect.Y,
						(int)drawCmd.ClipRect.Z - (int)drawCmd.ClipRect.X,
						(int)drawCmd.ClipRect.W - (int)drawCmd.ClipRect.Y);


					GuiShader.use();

					Gl.BindTexture(TextureTarget.Texture2D, (uint)drawCmd.TextureId);
					//set shader.
					//also set the vao. hmm.
					Gl.BindVertexArray(VAO);
					Gl.DrawElements(BeginMode.Triangles, (int)drawCmd.ElemCount, DrawElementsType.UnsignedShort, (IntPtr)(offset * sizeof(ushort)));
					Gl.BindVertexArray(0);
					//then incriment index
					offset += (int)drawCmd.ElemCount;
				}
			}
		}
		//so render command lists
		//okay so roughly what do I need to do. I need to get the vertex and index buffers out of the program and loaded into opengl.
		//then I need to loop over the draw commands and render them.
		//so the function 

	}
}
