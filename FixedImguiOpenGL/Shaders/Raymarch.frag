﻿#version 430 core

out vec4 FragColor;
out vec4 FragCompare;
in vec2 Coord;

subroutine float DistanceFunc(vec3 position, mat4 data);
subroutine float DistanceOperation(float dist1, float dist2, float r);

layout(location = 0) subroutine uniform DistanceFunc ActiveFunc[8];
//subroutine uniform DistanceFunc ActiveFunc;
layout(location = 8) subroutine uniform DistanceOperation ActiveOperation[6];
//subroutine uniform DistanceOperation ActiveOperation;
//hmm
//Distance Functions
subroutine(DistanceFunc) float SubDistSphere(vec3 position, mat4 data){
	//we know where radius is.
	return length(position)-data[0].x;
}
subroutine(DistanceFunc) float SubDistBox(vec3 position, mat4 data){
	vec3 halfBounds = data[0].xyz;
	vec3 q = abs(position) -halfBounds;
	return length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0);
}
//do I need this can I not just do round on all things.
subroutine(DistanceFunc) float SubDistRoundBox(vec3 position, mat4 data){
	vec3 halfBounds = data[0].xyz;
	float radius = data[0].w;

	vec3 q = abs(position) -halfBounds;
	return length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0) - radius;
}
subroutine(DistanceFunc) float SubDistTorus(vec3 position, mat4 data){
	vec2 radii = data[0].xy;
	
	vec2 q = vec2(length(position.xz) - radii.x,position.y);
	return length(q) - radii.y;
	//alright so I think what this is doing.
	//is finding the circle, or the radius on xz, but height of y.
	//and then after you have the distance of that circle you can find the torus by moving away from it with a radius
}
subroutine(DistanceFunc) float SubDistCone(vec3 position, mat4 data){
	vec2 sincos = data[0].xy;
	float height = data[0].z;

	vec2 q = height * vec2(sincos.x / sincos.y,-1);
	vec2 w = vec2(length(position.xz),position.y);//all of these assume y up, account for that.
	vec2 a = w - q * clamp(dot(w,q) / dot(q,q),0.0,1.0);
	vec2 b = w - q * vec2(clamp(w.x/q.x,0.0,1.0),1.0);
	float k = sign(q.y);
	float d = min(dot(a,a),dot(b,b));
	float s = max(k * (w.x * q.y - w.y * q.x),k * (w.y - q.y));
	return sqrt(d) * sign(s);
}
subroutine(DistanceFunc) float SubDistHexagonPrism(vec3 position, mat4 data){
	vec2 h = data[0].xy;

	const vec3 k = vec3(-.8660254,.5,.57735);//wonder what this is.
	position = abs(position);
	position.xy -= 2.0 * min(dot(k.xy,position.xy),0.0) * k.xy;
//	vec2 d = vec2(
//		length(position.xy - vec2(clamp(position.x,-k.z * h.x,k.z * h.x)) * sign(position.y - h.x),
//		position.z - h.y);

	vec2 d = vec2(
	    length(position.xy - vec2(clamp(position.x,-k.z * h.x,k.z * h.x), h.x)) * sign(position.y - h.x),
		position.z - h.y);
	return min(max(d.x,d.y),0.0) + length(max(d,0.0));

}
subroutine(DistanceFunc) float SubDistCapsule(vec3 position, mat4 data){
	float height = data[0].x;
	float radius = data[0].y;

	position.y -= clamp(position.y,0.0,height);

	return length(position) - radius;
}
subroutine(DistanceFunc) float SubDistCylinder(vec3 position, mat4 data){
	float height = data[0].x;
	float radius = data[0].y;

	vec2 d = abs(vec2(length(position.xz),position.y)) - vec2(height,radius);
	return min(max(d.x,d.y),0.0) + length(max(d,0.0));
}

//Operation Functions
subroutine (DistanceOperation) float SubOpUnion(float d1, float d2, float r){
	return min(d1,d2);	
}
subroutine (DistanceOperation) float SubOpDifference(float d1, float d2, float r){
	return max(-d1,d2);
}
subroutine (DistanceOperation) float SubOpIntersection(float d1, float d2, float r){
	return max(d1,d2);
}
subroutine (DistanceOperation) float SubOpSmoothUnion (float d1, float d2, float r){
	float h = clamp(0.5 + 0.5 * (d2-d1)/r,0.0,1.0);
	return mix(d2,d1,h) - r*h*(1.0-h);
}
subroutine (DistanceOperation) float SubOpSmoothDifference(float d1, float d2, float r){
	float h = clamp(0.5 - 0.5 * (d2+d1)/r,0.0,1.0);
	return mix(d2,-d1,h) + r*h * (1.0-h);
}
subroutine (DistanceOperation) float SubOpSmoothIntersection(float d1, float d2, float r){
	float h = clamp(0.5 - 0.5 * (d2-d1)/r,0.0,1.0);
	return mix(d2,d1,h)+ r*h *(1.0-h);
}

uniform float StartDistance = 1;
uniform float MaxDistance = 100;
uniform float Epsilon = .001;
uniform float FOV = tan(radians(45./2));
uniform float Aspect = 800./480.;
uniform vec2 HalfResolution = vec2(800,480);

uniform mat4 InverseView;

struct FieldData
{
	int FunctionIndex;
	int OperationIndex;
	float OperationRadius;
	float padding;
	mat4 InverseMatrix;
	mat4 DistanceProperties;
};

layout(binding = 0,std430) buffer FieldCollection
{
	FieldData fields[];
};
uniform int FieldCount;

float SceneSDF(vec3 position)
{
	float Distance = MaxDistance;
	vec3 iPosition;
	float Dist;
	for(int i = 0;i<FieldCount;i++)
	{
		iPosition = (vec4(position,1) * fields[i].InverseMatrix * InverseView).xyz;
		float Dist = ActiveFunc[fields[i].FunctionIndex](iPosition,fields[i].DistanceProperties);
		Distance = ActiveOperation[fields[i].OperationIndex](Dist,Distance,fields[i].OperationRadius);
	}

	return Distance;
}

vec3 estimateNormal(vec3 pos)
{
	float d = SceneSDF(pos);
	vec2 e = vec2(.01,0);
	vec3 n = d -vec3(
		SceneSDF(pos - e.xyy),
		SceneSDF(pos - e.yxy),
		SceneSDF(pos - e.yyx));
	return normalize(n);
}
float rayMarch(vec3 viewPos,vec3 viewDir)
{
	float depth = StartDistance;
	for(int i = 0;i<64;i++)
	{
		float dist = SceneSDF(viewPos + depth * viewDir);
		depth += dist;
		if(depth > MaxDistance || dist < Epsilon)
			break;
	}
	return depth;
}

vec3 rayDirection(vec2 coord)
{
	float yScale = FOV;
	vec2 xy = (coord * yScale) * vec2(Aspect,1);
	return normalize(vec3(xy,-1));
}

void main()
{
	vec2 calcCoord = (gl_FragCoord.xy - HalfResolution)/HalfResolution;
	vec3 viewRay = rayDirection(calcCoord);
	vec3 viewPosition = vec3(0,0,0);
	float d = rayMarch(viewPosition,viewRay);
//
	if(d >= MaxDistance)
		discard;
	vec3 collidePos = viewPosition + viewRay * d;
	vec3 normal = estimateNormal(collidePos);

	normal = (normal + 1) * .5f;
	
	FragColor = vec4(normal,1);
//	FragColor = vec4(Coord,gl_FragCoord.xy / gl_FragCoord.zw);
	

}