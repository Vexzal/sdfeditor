﻿#version 430 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 UV;
//just position
//right now it's just going to be a full screen quads

out vec2 Coord;

//hmm. for now juts pass in a projection oriented quad.
//Don't care about anything else.

void main()
{
	gl_Position = vec4(position/2,1) ;
	Coord = position.xy/2;
}
