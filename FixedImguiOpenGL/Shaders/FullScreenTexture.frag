﻿#version 430 core

out vec4 FragColor;

in vec2 TextureCoord;

uniform sampler2D userTexture;

void main()
{
	FragColor = texture(userTexture,TextureCoord);
	
}
