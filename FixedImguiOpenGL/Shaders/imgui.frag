﻿#version 430 core

out vec4 FragColor;

in vec2 UV;
in vec4 Color;

uniform sampler2D guiImage;

void main()
{
	FragColor = texture(guiImage,UV) * Color;
	
}


