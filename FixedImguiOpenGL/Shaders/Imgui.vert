﻿#version 330 core

layout(location = 0) in vec2 position;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec4 color;

out vec2 UV;
out vec4 Color;

uniform mat4 Projection;

void main()
{
	gl_Position = Projection * vec4(position,0,1) ;
	UV = uv;
	Color = color;
}