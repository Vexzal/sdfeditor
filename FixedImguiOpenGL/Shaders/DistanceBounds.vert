﻿#version 430 core

layout (location = 0) in vec3 position;
layout(location = 1) in float Index;

out vec2 Coord;

//alright.
//subroutines.
//I need a transform subroutine that modifies the vertex to align to the. the.
//to get the bounding box to align with the shape bounds.
//calculated through field properties and radius.
//same way as bounding boxes.
//I was about to talk about how to handle the camera but screw that I don't have to.
//I don't need to worry about that in the subroutine. the subroutine just translates the unit box into a bounds box.


subroutine vec3 BoundsFunc(vec3 position, mat4 data);

layout(location = 0) subroutine uniform BoundsFunc BoundsFunction[8];


///data layout
///[radius,-- ]
subroutine(BoundsFunc) vec3 SphereBounds(vec3 position, mat4 data)
{
	return position * (data[0].x * 2);//though this should be correct.the x y z values should. they aren't normalized. and need to be radius.
	//just, trans all these funcs for now.
	//(just returned position when I wrote this)
}
//[halfx,halfy,halfz,--]
subroutine(BoundsFunc) vec3 BoxBounds(vec3 position, mat4 data)
{
	//all the data here is half bounds but my box is unit 1 so I need to double values.
	vec3 bounds = data[0].xyz * 2;
	//so these are similarly radii value so.
	//I just have to remember the orientation these things are in.
	return position * bounds;
}
//I think torus uses the same. cylidrical values.
//box is same as round box I think.
//also uh, store radius with cone.
//I don't want to find radius from the sincos.

//alright so torus layout
///[radius 1, radius 2,--] //figure out whihc is major and minor or if it's inner outer whatever.
subroutine(BoundsFunc) vec3 TorusBounds(vec3 position, mat4 data)
{
	//I think y is a minor radius. and 
	//alright so wdith =
	vec2 radii = data[0].xy;
	float width = radii.x + radii.y;
	float height = radii.y;
	vec3 bounds = vec3(width*2,height*2,width*2);
	//wrong but fine for now.
	//in fact very wrong height is z. should be y. fixed
	return position * bounds;
}
///[sine, cosine, height, -] [angle,radius]
subroutine (BoundsFunc) vec3 ConeBounds(vec3 position,mat4 data)
{
	float diameter = data[1].y * 2;
	vec3 bounds = vec3(diameter,data[0].z,diameter);
	return position * bounds;
}
///[height 1, height 2,--] I think. it should be that height one is y height and h 2 is leg length.
subroutine(BoundsFunc) vec3 HexagonBounds(vec3 position, mat4 data)
{
	float leg = data[0].y *2;
	vec3 bounds = vec3(leg,data[0].x * 2,leg);
	return position * bounds;
	//alright here is the thing.
	//basicaly all of these operate in a half bound space.
	//this one absolutely does.
	//and I should treat it as such.
	//might come through and rewrite all of this to assume that data is half information.
}
subroutine(BoundsFunc) vec3 CapsuleBounds(vec3 position,mat4 data)
{
	float diameter =  data[0].y * 2;
	float height = data[0].x + diameter;
	vec3 bounds = vec3(diameter,height,diameter);

	return position * bounds;	
}
subroutine(BoundsFunc) vec3 CylinderBounds(vec3 position, mat4 data)
{
	float diameter = data[0].y * 2;
	float height = data[0].x;
	vec3 bounds = vec3(diameter,height,diameter);
	return position * bounds;
}
//no rounded box. uh. just use box for now i guess?

//hmm. Inverse transform View works fine normally but here it's.
//a bit of an issue.
//it works in the raymarcher because I want transforms happening in world space.
//add structs for storage buffer.

struct FieldDataVertex
{
	int Functionindex;
	int OperationIndex;
	float OperationRadius;
	float padding;
	mat4 InverseMatrix;
	mat4 DistanceProperties;
};
layout(binding = 0,std430)buffer FieldInformation
{
	FieldDataVertex vFields[];
};

uniform int[] indexMap;//maps instance to index;//look into adding this as a instance attribute.

uniform mat4 View;


uniform mat4 Projection;

void main()
{
	
	//instances.
//	int id = gl_InstanceID;
	//then from here.
	//it's a matter of.

//	int index = indexMap[id];
	int i = int(Index);

	vec3 modified = BoundsFunction[vFields[i].Functionindex](position,vFields[i].DistanceProperties);

	mat4 Transform = inverse(transpose( vFields[i].InverseMatrix));
	vec4 pos = Projection * View * Transform * vec4(modified,1);
	gl_Position = pos;
	
	Coord = pos.xy/pos.w;
	
	
//	gl_Position = Projection * View	 * vec4(position * 4,1);

}