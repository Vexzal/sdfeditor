﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//this is 
//to get some extra behaviour for using systems numerics types with open gl.
//or any other types.
using System.Numerics;

namespace ImguiOpenGL
{
	static class GLExtensions
	{
		public static float[] ToArrayColumn(this Matrix4x4 m)
		{
			return new float[16]
			{
				m.M11,m.M21,m.M31,m.M41,
				m.M12,m.M22,m.M32,m.M42,
				m.M13,m.M23,m.M33,m.M43,
				m.M14,m.M24,m.M34,m.M44
			};
			
		}
		public static float[] ToArrayRow(this Matrix4x4 m)
		{
			return new float[16] 
				{m.M11,m.M12,m.M13,m.M14,
				 m.M21,m.M22,m.M23,m.M24,
				 m.M31,m.M32,m.M33,m.M34,
				 m.M41,m.M42,m.M43,m.M44};
		}
		public static Vector4[] ToVector(this Matrix4x4 m )
		{
			return new Vector4[4]
			{
				new Vector4(m.M11,m.M12,m.M13,m.M14),
				new Vector4(m.M21,m.M22,m.M23,m.M24),
				new Vector4(m.M31,m.M32,m.M33,m.M34),
				new Vector4(m.M41,m.M42,m.M43,m.M44)
			};
		}
		public static Matrix4x4 Invert(this Matrix4x4 m)
		{
			Matrix4x4 result;

			Matrix4x4.Invert(m, out result);
			return result;
		}

		public static Matrix4x4 InfinitePerspectiveFieldOfView(float FOV, float aspect, float near)
		{
			float e = 1f / (float)Math.Tan(FOV / 2);

			float eOverA = e / aspect;

			return new Matrix4x4(
				e, 0, 0, 0,
				0, eOverA, 0, 0,
				0, 0, -1, -2 * near,
				0, 0, -1, 0);

		}

	}
}
