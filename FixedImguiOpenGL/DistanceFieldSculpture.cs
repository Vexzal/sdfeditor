﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace ImguiOpenGL
{
	class DistanceFieldSculpture
	{
		//transforms.
		//fuck.
		List<FieldData> data = new List<FieldData>();

		public DistanceFieldSculpture()
		{
			//first operation must be union or it's useless.
			//smooth union wouldn't do anything.
			//difference and intersection fail automatically.
			
			data.Add(new FieldData(FieldFunction.Sphere, FieldOperation.Union, 0, Matrix4x4.Identity, new Matrix4x4() { M11 = 1 }));
		}

	}
	public struct TransformData
	{
		public static TransformData Default => new TransformData(Vector3.Zero, Quaternion.Identity, Vector3.One);
		

		public Vector3 Position;
		public Quaternion Rotation;
		public Vector3 Scale;

		public Matrix4x4 MRotation => Matrix4x4.CreateFromQuaternion(Rotation);
		public Matrix4x4 Transform => Matrix4x4.CreateTranslation(Position) * MRotation * Matrix4x4.CreateScale(Scale);
		public Matrix4x4 Inverse()
		{
			Matrix4x4 inverse;
			Matrix4x4.Invert(Transform, out inverse);
			return inverse;
		}

		public TransformData(Vector3 position, Quaternion rotation, Vector3 scale)
		{
			Position = position;
			Rotation = rotation;
			Scale = scale;
		}
		public TransformData(Vector3 position, Matrix4x4 rotation,Vector3 scale )
		{
			Position = position;
			Rotation = Quaternion.CreateFromRotationMatrix(rotation);
			Scale = scale;
		}
		

	}
}
