﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GLFW;
using OpenGL;
using ImGuiNET;
using System.Numerics;

namespace ImguiOpenGL
{
	class Program
	{
		static void Main(string[] args)
		{
			using (var app = new Environment())
			{
				app.Run();
			}
		}
		static void Examp()
		{
			Matrix4x4 View = Matrix4x4.CreateLookAt(Vector3.UnitZ * 10, Vector3.Zero, Vector3.UnitY);
			Matrix4x4 Translation = Matrix4x4.CreateTranslation(1, 2, 4);

			Matrix4x4 inverseBoth;
			Matrix4x4 inverseView;
			Matrix4x4 inverseTranslation;
			Matrix4x4 inverseComposed;


			Matrix4x4.Invert(View, out inverseView);
			Matrix4x4.Invert(Translation, out inverseTranslation);
			Matrix4x4.Invert(Translation * View, out inverseBoth);

			inverseComposed = inverseTranslation * inverseView;

			if (inverseBoth == inverseComposed)
			{
				Console.WriteLine("equal");
			}


		}
	}

	class Environment : IDisposable
	{
		imguiRenderer imguiRenderer;
		Window window;
		Vector2 Resolution;
		Raymarcher raymarch = new Raymarcher();
		public void Run()
		{
			
			Glfw.Init();
			Resolution = new Vector2(1600, 960);
			Glfw.WindowHint(Hint.ClientApi, ClientApi.OpenGL);
			Glfw.WindowHint(Hint.ContextVersionMajor, 4);
			Glfw.WindowHint(Hint.ContextVersionMinor, 3);
			Glfw.WindowHint(Hint.OpenglProfile, Profile.Core);
			
			window = Glfw.CreateWindow((int)Resolution.X, (int)Resolution.Y, "Editor", Monitor.None, Window.None);
			Glfw.SetWindowSizeLimits(window, (int)Resolution.X, (int)Resolution.Y, (int)Resolution.X, (int)Resolution.Y);
			Glfw.MakeContextCurrent(window);
			Gl.Viewport(0, 0, (int)Resolution.X, (int)Resolution.Y);
			Glfw.SetKeyCallback(window, KeyboardCallback);
			//frame, screw it turn off resizing.
			imguiRenderer = new imguiRenderer(Resolution);

			Glfw.SetScrollCallback(window,
				(IntPtr window, double x, double y) =>
				{
					var io = ImGui.GetIO();
					io.MouseWheel = (float)x;
					io.MouseWheelH = (float)y;
				});
			raymarch.Load();

			while (!Glfw.WindowShouldClose(window))
			{
				Glfw.PollEvents();
				Update();
				Render();
				Glfw.SwapBuffers(window);
			}
		}
		
		public void Update()
		{
			raymarch.Update();
			imguiRenderer.UpdateInput(window);
		}
		bool everSetPositionSize = false;
		public void Render()
		{
			Gl.ClearColor(1f, .5f, .5f, 1f);
			Gl.Clear(ClearBufferMask.ColorBufferBit);

			raymarch.Render();
			#region Imgui;
			//UI();
			#endregion
		}
		void UI()
		{
			imguiRenderer.BeforeLayout();
			if (!everSetPositionSize)
			{
				ImGui.SetNextWindowPos(new Vector2(0, 0));
				ImGui.SetNextWindowSize(new Vector2(100, 100));
				everSetPositionSize = true;
			}
			ImGui.Begin("TitleWindow");
			//ImGui.LabelText("Hello World", "");
			for (int i = 0; i < 10; i++)
			{
				if (ImGui.Button("Button" + i))
				{
					Console.WriteLine(i);
					//System.Environment.Exit(0);
				}
			}
			if (ImGui.Button("ResetPosition"))
			{
				ImGui.SetWindowPos(new Vector2(0, 0));
				ImGui.SetWindowSize(new Vector2(100, 100));
			}

			ImGui.End();


			imguiRenderer.AfterLayout();
		}
		Vector2 CursorPosition = new Vector2();
		public void ProcessInput(Window window)
		{
			//hmm. I might just grab input whenever instead of cycling the whole frame into values.
			//mainly because I don't want to do otherwise.
			double x, y;
			Glfw.GetCursorPosition(window, out x, out y);
			CursorPosition = new Vector2((float)x, (float)y);

			
			
		}
		
		public void KeyboardCallback(IntPtr window,Keys keyCode, int scanCode, InputState inputState, ModifierKeys modifierKeys)
		{
			
		}
		
		public void Dispose()
		{
			
		}
	}
}
