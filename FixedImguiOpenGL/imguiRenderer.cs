﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenGL;
using ImGuiNET;
using System.Numerics;
using System.Runtime.InteropServices;
using GLFW;

namespace ImguiOpenGL
{
	public class imguiRenderer
	{
		const int imDrawSize = sizeof(float) * 4 + sizeof(uint);		
		uint VAO;
		uint VBO;
		uint EBO;
		uint FontTexture;

		Shader GuiShader;
		Matrix4x4 Projection;
		
		Vector2 Resolution;
		
		public imguiRenderer(Vector2 resolution)
		{
			var context = ImGui.CreateContext();
			ImGui.SetCurrentContext(context);

			Resolution = resolution;

			GuiShader = new Shader(@"Shaders\imgui.vert", @"Shaders\imgui.frag");

			Projection = Matrix4x4.CreateOrthographicOffCenter(0, resolution.X, resolution.Y, 0, -1, 1);		

			GuiShader.use();
			Gl.UniformMatrix4fv(Gl.GetUniformLocation(GuiShader.ID, "Projection"), 1, false, Projection.ToArrayRow());
			Gl.UseProgram(0);			
			
			VAO = Gl.GenVertexArray();
			VBO = Gl.GenBuffer();
			EBO = Gl.GenBuffer();
			Gl.BindVertexArray(VAO);
			Gl.BindBuffer(BufferTarget.ArrayBuffer, VBO);
			Gl.BindBuffer(BufferTarget.ElementArrayBuffer, EBO);			

			Gl.VertexAttribPointer(0, 2, VertexAttribPointerType.Float, false, imDrawSize, (IntPtr)0);			
			Gl.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, imDrawSize, (IntPtr)(sizeof(float) * 2));	
			Gl.VertexAttribPointer(2, 4, VertexAttribPointerType.UnsignedByte, true, imDrawSize, (IntPtr)(sizeof(float) * 4));
			
			Gl.EnableVertexAttribArray(0);
			Gl.EnableVertexAttribArray(1);
			Gl.EnableVertexAttribArray(2);
			//hmm.
			//define 
			Gl.BindVertexArray(0);


			BindAtlas();
			ImGui.NewFrame();

			var io = ImGui.GetIO();
			io.DisplaySize = resolution;
			io.DisplayFramebufferScale = new Vector2(1, 1);

		}
		public void UpdateInput(Window w)
		{
			//skipped this but shouldn't of.
			var io = ImGui.GetIO();
			//it includes updates I care about.

			double x, y;

			Glfw.GetCursorPosition(w, out x, out y);
			io.MousePos = new Vector2((float)x, (float)y);


			io.MouseDown[0] = Glfw.GetMouseButton(w, MouseButton.Left) == InputState.Press;
			io.MouseDown[1] = Glfw.GetMouseButton(w, MouseButton.Right) == InputState.Press;
			io.MouseDown[2] = Glfw.GetMouseButton(w, MouseButton.Middle) == InputState.Press;

		}
		

		//create an id for a texture.
		public IntPtr BindTexture(uint TextureId)
		{
			return (IntPtr)TextureId;
		}
		
		//binds the font atlas to the gpu.
	    void BindAtlas()
		{
			var fonts = ImGui.GetIO().Fonts;
			ImGui.GetIO().Fonts.AddFontDefault();
			var io = ImGui.GetIO();
			IntPtr pixelData = IntPtr.Zero;
			io.Fonts.GetTexDataAsRGBA32(out pixelData, out int width, out int height, out int bytesPerPixel);
			
			//io.Fonts.SetTexID((IntPtr))
			
			FontTexture = Gl.GenTexture();
			Gl.BindTexture(TextureTarget.Texture2D,FontTexture);
			Gl.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba32f, width, height, 0, PixelFormat.Rgba, PixelType.UnsignedByte,pixelData);
			Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, TextureParameter.ClampToEdge);
			Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, TextureParameter.ClampToEdge);
			Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, TextureParameter.Linear);
			Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, TextureParameter.Linear);
			Gl.BindTexture(TextureTarget.Texture2D, 0);
			
			io.Fonts.SetTexID((IntPtr)FontTexture);
			io.Fonts.ClearTexData();
			
			
		}
		
		public void BeforeLayout()
		{
			ImGui.GetIO().DeltaTime = /*(float)GLFW.Glfw.Time;*/ 1f / 60f;
			
			ImGui.NewFrame();
		}
		public void AfterLayout()
		{
			ImGui.Render();
			RenderDrawData(ImGui.GetDrawData());
			
		}
		void RenderDrawData(ImDrawDataPtr drawData)
		{		
			UpdateBufers(drawData);
			RenderCommands(drawData);



		}		
		void UpdateBufers(ImDrawDataPtr drawData)
		{
			if (drawData.TotalVtxCount == 0)
				return;
			
			Gl.BindBuffer(BufferTarget.ArrayBuffer, VBO);
			Gl.BindBuffer(BufferTarget.ElementArrayBuffer, EBO);
			int vtxOffset = 0;
			int idxOffset = 0;
		
			Gl.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(drawData.TotalVtxCount * imDrawSize), IntPtr.Zero, BufferUsageHint.StreamDraw);
			Gl.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(drawData.TotalIdxCount * sizeof(ushort)), IntPtr.Zero, BufferUsageHint.StreamDraw);
			

			for (int i = 0; i < drawData.CmdListsCount; i++)
			{
				var cmdList = drawData.CmdListsRange[i];
				Gl.BufferSubData(BufferTarget.ArrayBuffer, (IntPtr)(vtxOffset * imDrawSize), (IntPtr)(cmdList.VtxBuffer.Size * imDrawSize), cmdList.VtxBuffer.Data);
				Gl.BufferSubData(BufferTarget.ElementArrayBuffer, (IntPtr)(idxOffset * sizeof(ushort)), (IntPtr)(cmdList.IdxBuffer.Size * sizeof(ushort)), cmdList.IdxBuffer.Data);

				vtxOffset += cmdList.VtxBuffer.Size;
				idxOffset = cmdList.IdxBuffer.Size;
			}
			Gl.BindBuffer(BufferTarget.ArrayBuffer, 0);
			Gl.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
			
			

		}
		
		void RenderCommands(ImDrawDataPtr drawData)
		{
			
			int offset = 0;
			Gl.Disable(EnableCap.DepthTest);
			Gl.Enable(EnableCap.Blend);
			Gl.BlendEquation(BlendEquationMode.FuncAdd);
			Gl.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
			Gl.Enable(EnableCap.ScissorTest);			
			

			drawData.ScaleClipRects(ImGui.GetIO().DisplayFramebufferScale);

			for (int list = 0;list < drawData.CmdListsCount;list++)
			{				

				ImDrawListPtr cmdList = drawData.CmdListsRange[list];
								

				for (int cmd = 0;cmd < cmdList.CmdBuffer.Size;cmd++)
				{
					var drawCmd = cmdList.CmdBuffer[cmd];
					
					int clipOffset = (int)drawCmd.ClipRect.W + (int)drawCmd.ClipRect.Y;
					int clipWidth = (int)drawCmd.ClipRect.Z - (int)drawCmd.ClipRect.X;
					int clipHeight = (int)drawCmd.ClipRect.W - (int)drawCmd.ClipRect.Y;									

					Gl.Scissor((int)drawCmd.ClipRect.X,
						(int)Resolution.Y - (int)drawCmd.ClipRect.W,
						clipWidth,
						clipHeight);
					

					GuiShader.use();
					
					//Gl.UniformMatrix4fv(Gl.GetUniformLocation(GuiShader.ID, "Projection"), ProjectionGL);
					Gl.UniformMatrix4fv(Gl.GetUniformLocation(GuiShader.ID, "Projection"), 1, false, Projection.ToArrayRow());
					Gl.BindTexture(TextureTarget.Texture2D, (uint)drawCmd.TextureId);
					
					Gl.BindVertexArray(VAO);
					
					
					Gl.DrawElements(BeginMode.Triangles, (int)drawCmd.ElemCount, DrawElementsType.UnsignedShort, (IntPtr)(offset * sizeof(ushort)));
					Gl.BindVertexArray(0);
					
					offset += (int)drawCmd.ElemCount;
				}
			}
			Gl.Disable(EnableCap.ScissorTest);
			Gl.Disable(EnableCap.Blend);
		}		
	}
}
