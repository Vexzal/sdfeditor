﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImGuiNET;
using OpenGL;
using GLFW;
using System.Numerics;

namespace FixedImguiOpenGL
{
	class FromCimguiRenderer
	{
		public FromCimguiRenderer()
		{

		}

		uint VAO, VBO, EBO;
		uint FontTexture;

		Shader GuiShader;

		Matrix4x4 Projection;

		public void Render(ImDrawDataPtr data, int width, int height)
		{
			Gl.Enable(EnableCap.Blend);
			Gl.BlendEquation(BlendEquationMode.FuncAdd);
			Gl.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
			Gl.Disable(EnableCap.CullFace);
			Gl.Disable(EnableCap.DepthTest);
			Gl.Enable(EnableCap.ScissorTest);

			Gl.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);

			Projection = Matrix4x4.CreateOrthographicOffCenter(0, width, height, 0, -1, 1);

			
			

		}

	}
}
