﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenGL;
using GLFW;
using ImGuiNET;
using System.Numerics;

namespace ImguiOpenGL
{

	//before I get to far I want to write down an idea.
	//I could 


	public enum FieldFunction : int
	{
		Sphere = 0,
		Box = 1,
		RoundBox = 2,
		Torus = 3,
		Cone = 4,
		HexagonPrism = 5,
		Capsule = 6,
		Cylinder = 7
	}
	public enum FieldOperation : int
	{
		Union = 0,
		Difference = 1,
		Intersection = 2,
		SmoothUnion = 3,
		SmoothDifference = 4,
		SmoothIntersection = 5
	}

	public struct FieldData
	{
		public static int SizeOf = sizeof(float) * 34 + sizeof(int) * 2;
		FieldFunction Functionindex;
		FieldOperation OperationIndex;
		float Radius;
		float padding;
		Matrix4x4 InverseMatrix;
		Matrix4x4 DistanceProperties;
		
		public FieldData(FieldFunction functionIndex, FieldOperation operationIndex, float radius, Matrix4x4 inverse, Matrix4x4 properties)
		{
			Functionindex = functionIndex;
			OperationIndex = operationIndex;
			Radius = radius;
			padding = 0;
			InverseMatrix =  Matrix4x4.Transpose( inverse);
			DistanceProperties = Matrix4x4.Transpose( properties);
		}
	}
	//main program.
	//I need to manage selection somewhere huh.
	class Raymarcher
	{
		public Vector2 Resolution = new Vector2(1600,900);

		uint Texture;
		uint RaymarchSSBO;

		Shader RaymarchShader;
		Shader FullScreen;
		Shader RaymarchFrag;

		uint fullScreenVAO;

		Matrix4x4 View;

		const float ToRadians = (float)(Math.PI / 180.0);
		//for now just use field data array.
		FieldData[] FieldCollection = new FieldData[2];

		uint[] fragSubroutine;


		//instance variables.
		Shader BoundsShader;
		uint cubeVAO;
		uint instanceVBO;
		uint boundsTexture;//ignore this for now; it needs to be bound as a render target later but I just want to draw to screen for now
		int cubeIndexCount;
		uint boundsFBO;

		Matrix4x4 Projection;


		uint[] cubeSubroutines;

		public void Load()
		{
			View = Matrix4x4.CreateLookAt(Vector3.UnitZ * 10, Vector3.Zero, Vector3.UnitY);

			#region Shaders
			{
				FullScreen = new Shader(@"Shaders\FullScreenTexture.vert", @"Shaders\FullScreenTexture.frag");
				RaymarchShader = new Shader(@"Shaders\PureRaymarch.comp");

				RaymarchShader.use();
				var FieldSubroutine = Gl.GetSubroutineUniformLocation(RaymarchShader.ID, ShaderType.ComputeShader, "ActiveFunc");
				var OperationSubroutine = Gl.GetSubroutineUniformLocation(RaymarchShader.ID, ShaderType.ComputeShader, "ActiveOperation");

				subroutineIndices = new uint[14];
				subroutineIndices[FieldSubroutine] = Gl.GetSubroutineIndex(RaymarchShader.ID, ShaderType.ComputeShader, "SubDistSphere");
				subroutineIndices[FieldSubroutine + 1] = Gl.GetSubroutineIndex(RaymarchShader.ID, ShaderType.ComputeShader, "SubDistBox");
				subroutineIndices[FieldSubroutine + 2] = Gl.GetSubroutineIndex(RaymarchShader.ID, ShaderType.ComputeShader, "SubDistRoundBox");
				subroutineIndices[FieldSubroutine + 3] = Gl.GetSubroutineIndex(RaymarchShader.ID, ShaderType.ComputeShader, "SubDistTorus");
				subroutineIndices[FieldSubroutine + 4] = Gl.GetSubroutineIndex(RaymarchShader.ID, ShaderType.ComputeShader, "SubDistCone");
				subroutineIndices[FieldSubroutine + 5] = Gl.GetSubroutineIndex(RaymarchShader.ID, ShaderType.ComputeShader, "SubDistHexagonPrism");
				subroutineIndices[FieldSubroutine + 6] = Gl.GetSubroutineIndex(RaymarchShader.ID, ShaderType.ComputeShader, "SubDistCapsule");
				subroutineIndices[FieldSubroutine + 7] = Gl.GetSubroutineIndex(RaymarchShader.ID, ShaderType.ComputeShader, "SubDistCylinder");
				subroutineIndices[OperationSubroutine] = Gl.GetSubroutineIndex(RaymarchShader.ID, ShaderType.ComputeShader, "SubOpUnion");
				subroutineIndices[OperationSubroutine + 1] = Gl.GetSubroutineIndex(RaymarchShader.ID, ShaderType.ComputeShader, "SubOpDifference");
				subroutineIndices[OperationSubroutine + 2] = Gl.GetSubroutineIndex(RaymarchShader.ID, ShaderType.ComputeShader, "SubOpIntersection");
				subroutineIndices[OperationSubroutine + 3] = Gl.GetSubroutineIndex(RaymarchShader.ID, ShaderType.ComputeShader, "SubOpSmoothUnion");
				subroutineIndices[OperationSubroutine + 4] = Gl.GetSubroutineIndex(RaymarchShader.ID, ShaderType.ComputeShader, "SubOpSmoothDifference");
				subroutineIndices[OperationSubroutine + 5] = Gl.GetSubroutineIndex(RaymarchShader.ID, ShaderType.ComputeShader, "SubOpSmoothIntersection");

				Gl.UseProgram(0);


				RaymarchFrag = new Shader(@"Shaders\Raymarch.vert", @"Shaders\Raymarch.frag");

				RaymarchFrag.use();
				fragSubroutine = new uint[14];
				fragSubroutine[FieldSubroutine] = Gl.GetSubroutineIndex(RaymarchFrag.ID, ShaderType.FragmentShader, "SubDistSphere");
				fragSubroutine[FieldSubroutine + 1] = Gl.GetSubroutineIndex(RaymarchFrag.ID, ShaderType.FragmentShader, "SubDistBox");
				fragSubroutine[FieldSubroutine + 2] = Gl.GetSubroutineIndex(RaymarchFrag.ID, ShaderType.FragmentShader, "SubDistRoundBox");
				fragSubroutine[FieldSubroutine + 3] = Gl.GetSubroutineIndex(RaymarchFrag.ID, ShaderType.FragmentShader, "SubDistTorus");
				fragSubroutine[FieldSubroutine + 4] = Gl.GetSubroutineIndex(RaymarchFrag.ID, ShaderType.FragmentShader, "SubDistCone");
				fragSubroutine[FieldSubroutine + 5] = Gl.GetSubroutineIndex(RaymarchFrag.ID, ShaderType.FragmentShader, "SubDistHexagonPrism");
				fragSubroutine[FieldSubroutine + 6] = Gl.GetSubroutineIndex(RaymarchFrag.ID, ShaderType.FragmentShader, "SubDistCapsule");
				fragSubroutine[FieldSubroutine + 7] = Gl.GetSubroutineIndex(RaymarchFrag.ID, ShaderType.FragmentShader, "SubDistCylinder");
				fragSubroutine[OperationSubroutine] = Gl.GetSubroutineIndex(RaymarchFrag.ID, ShaderType.FragmentShader, "SubOpUnion");
				fragSubroutine[OperationSubroutine + 1] = Gl.GetSubroutineIndex(RaymarchFrag.ID, ShaderType.FragmentShader, "SubOpDifference");
				fragSubroutine[OperationSubroutine + 2] = Gl.GetSubroutineIndex(RaymarchFrag.ID, ShaderType.FragmentShader, "SubOpIntersection");
				fragSubroutine[OperationSubroutine + 3] = Gl.GetSubroutineIndex(RaymarchFrag.ID, ShaderType.FragmentShader, "SubOpSmoothUnion");
				fragSubroutine[OperationSubroutine + 4] = Gl.GetSubroutineIndex(RaymarchFrag.ID, ShaderType.FragmentShader, "SubOpSmoothDifference");
				fragSubroutine[OperationSubroutine + 5] = Gl.GetSubroutineIndex(RaymarchFrag.ID, ShaderType.FragmentShader, "SubOpSmoothIntersection");
				Gl.UseProgram(0);

			}
			#endregion
			#region ShaderStorage
			{
				Matrix4x4 properties = Matrix4x4.Identity;
				properties.M11 = properties.M22 = properties.M33 = properties.M44 = 0;
				properties.M11 = 2;

				Matrix4x4 inverseA;
				Matrix4x4 inverseB;

				Matrix4x4.Invert(/*Matrix4x4.CreateTranslation(0, 2, 0) **/ Matrix4x4.CreateTranslation(-3,0,0 ) ,out inverseA);
				Matrix4x4.Invert(/*Matrix4x4.CreateTranslation(0, 2, 0) **/ /*Matrix4x4.CreateRotationX(90 * ToRadians) * */Matrix4x4.CreateTranslation(3, 0, 0), out inverseB);

				properties.M11 = 2;
				properties.M12 = 2;
				//properties.M21 = 2;

				//cone.

				float coneHeight = 4;
				float coneRadius = 2;
				
				//float coneAngle = Math.Atan(coneHeight / coneRadius);

				//properties.M11 = (float)Math.Sin(Math.Atan2(coneRadius,coneHeight));
				//properties.M12 = (float)Math.Cos(Math.Atan2(coneRadius,coneHeight));
				//properties.M13 = 4;

				FieldCollection[0] = new FieldData(FieldFunction.Sphere, FieldOperation.Union, 0, inverseA, properties);
				//properties.M11 = 2;
				properties.M11 = 1.5f;
				properties.M12 = 2;
				FieldCollection[1] = new FieldData(FieldFunction.Sphere, FieldOperation.Union, 0, inverseB, properties);

				RaymarchSSBO = Gl.GenBuffer();
				Gl.BindBuffer(BufferTarget.ShaderStorageBuffer, RaymarchSSBO);
				Gl.BufferData(BufferTarget.ShaderStorageBuffer, FieldData.SizeOf * FieldCollection.Length, FieldCollection, BufferUsageHint.StreamDraw);
				Gl.BindBufferRange(BufferTarget.ShaderStorageBuffer, 0, RaymarchSSBO, IntPtr.Zero, (IntPtr)(FieldData.SizeOf * FieldCollection.Length));
				Gl.BindBuffer(BufferTarget.ShaderStorageBuffer, 0);
			}
			#endregion
			#region Textures
			{
				Texture = Gl.GenTexture();
				Gl.BindTexture(TextureTarget.Texture2D, Texture);
				Gl.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba32f, (int)Resolution.X, (int)Resolution.Y, 0, PixelFormat.Rgba, PixelType.Float, IntPtr.Zero);
				Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, TextureParameter.ClampToEdge);
				Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, TextureParameter.Linear);
				Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, TextureParameter.Linear);
				Gl.BindImageTexture(0, Texture, 0, false, 0, BufferAccess.WriteOnly, PixelInternalFormat.Rgba32f);
			}	
			#endregion
			#region Geoemtry
			{
				float[] fullScreenVerticies = new float[]
				{
					-1f,-1f,0f, 0,0,
					-1f,1f,0,   0,1,
					1f,-1f,0,   1,0,
					1f,1f,0,    1,1
				};
				uint[] fullScreenIndices = new uint[]
				{
					0,1,2,2,1,3
				};
				fullScreenVAO = Gl.GenVertexArray();

				uint VBO = Gl.GenBuffer();
				uint EBO = Gl.GenBuffer();

				Gl.BindVertexArray(fullScreenVAO);
				Gl.BindBuffer(BufferTarget.ArrayBuffer, VBO);
				Gl.BindBuffer(BufferTarget.ElementArrayBuffer, EBO);
				Gl.BufferData(BufferTarget.ArrayBuffer, sizeof(float) * fullScreenVerticies.Length, fullScreenVerticies, BufferUsageHint.StaticDraw);
				Gl.BufferData(BufferTarget.ElementArrayBuffer, sizeof(uint) * fullScreenIndices.Length, fullScreenIndices, BufferUsageHint.StaticDraw);
				Gl.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, sizeof(float) * 5, IntPtr.Zero);
				Gl.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, sizeof(float) * 5, (IntPtr)(sizeof(float) * 3));
				Gl.EnableVertexAttribArray(0);
				Gl.EnableVertexAttribArray(1);
				Gl.BindVertexArray(0);
			}
			#endregion
			#region BoundsInstancing
			{
				Projection = Matrix4x4.CreatePerspectiveFieldOfView(ToRadians * 45, 1600.0f / 960.0f, 1, 100);

				BoundsShader = new Shader(@"Shaders/DistanceBounds.vert", @"Shaders/Raymarch.frag");

				uint[] instanceIndices = new uint[] { 1, 0 };//since it's both shapes I need both indicies;


				Vector3[] cubeVerts;
				int[] cubeIndex;

				Vector3 unit = Vector3.One / 2;
				//unit += new Vector3(.2f, .2f, .2f);
				CubeFromBounds(-unit, unit, out cubeVerts, out cubeIndex);
				cubeIndexCount = cubeIndex.Length;
				cubeVAO = Gl.GenVertexArray();
				instanceVBO = Gl.GenBuffer();
				uint VBO = Gl.GenBuffer();
				uint EBO = Gl.GenBuffer();

				Gl.BindVertexArray(cubeVAO);
				Gl.BindBuffer(BufferTarget.ArrayBuffer, VBO);
				Gl.BindBuffer(BufferTarget.ElementArrayBuffer, EBO);

				//data.
				Gl.BufferData(BufferTarget.ArrayBuffer, sizeof(float) * 3 * cubeVerts.Length, cubeVerts, BufferUsageHint.StaticDraw);
				Gl.BufferData(BufferTarget.ElementArrayBuffer, sizeof(int) * cubeIndex.Length, cubeIndex, BufferUsageHint.StaticDraw);

				//vertex attribs
				Gl.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 0, (IntPtr)0);
				Gl.EnableVertexAttribArray(0);
				Gl.EnableVertexAttribArray(1);
				Gl.BindBuffer(BufferTarget.ArrayBuffer, instanceVBO);
				Gl.BufferData(BufferTarget.ArrayBuffer, sizeof(uint) * instanceIndices.Length, instanceIndices, BufferUsageHint.StreamDraw);
				Gl.VertexAttribPointer(1, 1, VertexAttribPointerType.UnsignedInt, false, 0, (IntPtr)0);
				
				Gl.BindBuffer(BufferTarget.ArrayBuffer, 0);
				Gl.VertexAttribDivisor(1, 1);
				Gl.BindVertexArray(0);
				//alright. welcome to hell.
				cubeSubroutines = new uint[8];
				cubeSubroutines[0] = Gl.GetSubroutineIndex(BoundsShader.ID, ShaderType.VertexShader, "SphereBounds");
				cubeSubroutines[1] = Gl.GetSubroutineIndex(BoundsShader.ID, ShaderType.VertexShader, "BoxBounds");
				cubeSubroutines[2] = Gl.GetSubroutineIndex(BoundsShader.ID, ShaderType.VertexShader, "BoxBounds");
				cubeSubroutines[3] = Gl.GetSubroutineIndex(BoundsShader.ID, ShaderType.VertexShader, "TorusBounds");
				cubeSubroutines[4] = Gl.GetSubroutineIndex(BoundsShader.ID, ShaderType.VertexShader, "ConeBounds");
				cubeSubroutines[5] = Gl.GetSubroutineIndex(BoundsShader.ID, ShaderType.VertexShader, "HexagonBounds");
				cubeSubroutines[6] = Gl.GetSubroutineIndex(BoundsShader.ID, ShaderType.VertexShader, "CapsuleBounds");
				cubeSubroutines[7] = Gl.GetSubroutineIndex(BoundsShader.ID, ShaderType.VertexShader, "CylinderBounds");

				//I could make this as a monotone but for now this is fine.
				//everything is the same BUT
				boundsFBO = Gl.GenFramebuffer();
				Gl.BindFramebuffer(FramebufferTarget.Framebuffer, boundsFBO);
				boundsTexture = Gl.GenTexture();
				Gl.BindTexture(TextureTarget.Texture2D, boundsTexture);
				Gl.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba32f, (int)Resolution.X, (int)Resolution.Y, 0, PixelFormat.Rgba, PixelType.Float, IntPtr.Zero);
				Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, TextureParameter.ClampToEdge);
				Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, TextureParameter.Linear);
				Gl.TexParameteri(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, TextureParameter.Linear);
				Gl.BindImageTexture(1, boundsTexture, 0, false, 0, BufferAccess.ReadWrite, PixelInternalFormat.Rgba32f);

				Gl.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, boundsTexture, 0);
				Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

				//alright so we have a texture. I need to do 2 things.
				//I need to bind it as a read only 
			}
			#endregion
			LoadHardCode();
		}
		public void GUI()
		{

		}
		public void Update()
		{
			//figure out camera rotation.
			//and camera movement.
			//mmmm.
		}
		Shader HardCodeShader;
		void LoadHardCode()
		{
			HardCodeShader = new Shader(@"Shaders\HardCodedRaymarch.comp");
		}
		void HardCodeCompute()
		{
			HardCodeShader.use();
			Matrix4x4 inverseA;
			Matrix4x4 inverseB;
			Matrix4x4.Invert(Matrix4x4.Identity * View, out inverseA);
			Matrix4x4.Invert(Matrix4x4.CreateTranslation(1, 1, 1) * View, out inverseB);

			Gl.UniformMatrix4fv(Gl.GetUniformLocation(HardCodeShader.ID, "inverseA"), 1, false, inverseA.ToArrayColumn());
			Gl.UniformMatrix4fv(Gl.GetUniformLocation(HardCodeShader.ID, "inverseB"), 1, false, inverseB.ToArrayColumn());
			Gl.Uniform1iv(Gl.GetUniformLocation(HardCodeShader.ID, "LoopCount"), 1, new int[] { 2 });


			Gl.DispatchCompute((uint)Resolution.X, (uint)Resolution.Y, 1);
		}
		uint[] subroutineIndices;
		void Compute()
		{
			RaymarchShader.use();


			Matrix4x4 viewInverse;
			Matrix4x4.Invert(View, out viewInverse);

			Gl.UniformMatrix4fv(Gl.GetUniformLocation(RaymarchShader.ID, "InverseView"), 1, false, viewInverse.ToArrayColumn());
			Gl.Uniform1fv(Gl.GetUniformLocation(RaymarchShader.ID, "Aspect"), 1, new float[] { Resolution.X / Resolution.Y });
			Gl.Uniform1iv(Gl.GetUniformLocation(RaymarchShader.ID, "FieldCount"), 1, new int[] { FieldCollection.Length });
			Gl.UniformSubroutinesuiv(ShaderType.ComputeShader, subroutineIndices.Length, subroutineIndices);

			Gl.DispatchCompute((uint)Resolution.X, (uint)Resolution.Y, 1);
		}

		void FragRaymarch()
		{
			RaymarchFrag.use();
			Gl.Uniform1fv(Gl.GetUniformLocation(RaymarchFrag.ID, "Aspect"), 1, new float[] { 1600f/ 960f});
			Gl.Uniform1iv(Gl.GetUniformLocation(RaymarchFrag.ID, "FieldCount"), 1, new int[] { FieldCollection.Length });
			Gl.UniformSubroutinesuiv(ShaderType.FragmentShader, fragSubroutine.Length, fragSubroutine);
			//nope.

			//matrix
			Matrix4x4 vInverse;
			Matrix4x4.Invert(View, out vInverse);
			Gl.UniformMatrix4fv((Gl.GetUniformLocation(RaymarchFrag.ID, "InverseView")), 1, false, vInverse.ToArrayColumn());

			//hope this works.
			Gl.BindVertexArray(fullScreenVAO);
			Gl.DrawElements(BeginMode.Triangles, 6, DrawElementsType.UnsignedInt, IntPtr.Zero);
			Gl.BindVertexArray(0);
		}

		void DrawBounds()
		{
			//assigne subroutines.
			Gl.BindFramebuffer(FramebufferTarget.Framebuffer, boundsFBO);

			Gl.ClearColor(0,0,0,0);
			Gl.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
			//Gl.Enable(EnableCap.DepthTest);
			BoundsShader.use();
			Matrix4x4 vInverse;
			Matrix4x4.Invert(View, out vInverse);
			Gl.UniformMatrix4fv((Gl.GetUniformLocation(BoundsShader.ID, "InverseView")), 1, false, vInverse.ToArrayColumn());
			Gl.Uniform1fv(Gl.GetUniformLocation(BoundsShader.ID, "Aspect"), 1, new float[] { 1600f / 960f });
			Gl.Uniform1iv(Gl.GetUniformLocation(BoundsShader.ID, "FieldCount"), 1, new int[] { FieldCollection.Length });
			Gl.UniformSubroutinesuiv(ShaderType.FragmentShader, fragSubroutine.Length, fragSubroutine);

			Gl.UniformMatrix4fv(Gl.GetUniformLocation(BoundsShader.ID, "View"), 1, true, View.ToArrayColumn());
			Gl.UniformMatrix4fv(Gl.GetUniformLocation(BoundsShader.ID, "Projection"), 1, false, Projection.ToArrayColumn());
			Gl.UniformSubroutinesuiv(ShaderType.VertexShader, cubeSubroutines.Length, cubeSubroutines);
			Gl.BindVertexArray(cubeVAO);			
			Gl.DrawElementsInstanced(BeginMode.Triangles, cubeIndexCount, DrawElementsType.UnsignedInt, (IntPtr)0, 2);
			Gl.BindVertexArray(0);

			//Gl.Disable(EnableCap.DepthTest);

			Gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
		}

		void DrawBuffer()
		{
			

			
			FullScreen.use();
			Gl.BindTexture(TextureTarget.Texture2D, boundsTexture);
			Gl.BindVertexArray(fullScreenVAO);
			Gl.DrawElements(BeginMode.Triangles, 6, DrawElementsType.UnsignedInt, IntPtr.Zero);
			Gl.BindVertexArray(0);
			Gl.BindTexture(TextureTarget.Texture2D, 0);
			
		}

		public void Render()
		{
			DrawBounds();

			//Compute();
			//HardCodeCompute();
			DrawBuffer();
			//FragRaymarch();

		}
		void CubeFromBounds(Vector3 min, Vector3 max,out Vector3[] vertices, out int[] indices)
		{
			CubeFromBounds(min.X, min.Y, min.Z, max.X, max.Y, max.Z, out vertices, out indices);
		}
		void CubeFromBounds(float i, float j, float k, float x, float y, float z,out Vector3[] vertices, out int[] indices)
		{
			vertices = new Vector3[8]
			{
				new Vector3(i,j,z),
				new Vector3(x,j,z),
				new Vector3(x,y,z),
				new Vector3(i,y,z),

				new Vector3(i,j,k ),
				new Vector3(x,j,k),
				new Vector3(x,y,k),
				new Vector3(i,y,k)
			};
			indices = new int[36]
			{
				0,1,2,
				2,3,0,
				1,5,6,
				6,2,1,
				7,6,5,
				5,4,7,
				4,0,3,
				3,7,4,
				4,5,1,
				1,0,4,
				3,2,6,
				6,7,3
			};
		}
	}
}
